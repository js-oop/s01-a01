//How do you create arrays in JS?
let students = ["John", "Joe", "Jane", "Jessie"]
console.log(typeof(students[0]))

//How do you access the first character of an array?
let firstString = students[0]
console.log(firstString.charAt(0))

//How do you access the last character of an array?
let lastString = students[students.length - 1]
console.log(lastString.charAt(lastString.length - 1))

//What array method searches for, and returns the index of a given value in an array? This method returns -1 if given value is not found in the array.
// findIndex()
// IndexOf()

//What array method loops over all elements of an array, performing a user-defined function on each iteration?
// forEach()

//What array method creates a new array with elements obtained from a user-defined function?
// map()

//What array method checks if all its elements satisfy a given condition?
// every()

//What array method checks if at least one of its elements satisfies a given condition?
// some()

//True or False: array.splice() modifies a copy of the array, leaving the original unchanged.
// False

//True or False: array.slice() copies elements from original array and returns them as a new array.
// True

// Create a function named addToEnd that will add a passed in string to the end of a passed in array. If element to be added is not a string, return the string "error - can only add strings to an array". Otherwise, return the updated array. Use the students array and the string "Ryan" as arguments when testing.
function addToEnd(end, array){
    if(typeof(end) != typeof(String())){
        console.log("error - can only add strings to an array")
    } else {
        array.push(end)
        console.log(array)
    }
}

addToEnd(5)
addToEnd("Ryan", students)

//Create a function named addToStart that will add a passed in string to the start of a passed in array. If element to be added is not a string, return the string "error - can only add strings to an array". Otherwise, return the updated array. Use the students array and the string "Tess" as arguments when testing.
function addToStart(start, array){
    if(typeof(start) != typeof(String())){
        console.log("error - can only add strings to an array")
    } else {
        array.unshift(start)
        console.log(array)
    }
}

addToStart(2, students)
addToStart("Tess", students)

//Create a function named elementChecker that will check a passed in array if at least one of its elements has the same value as a passed in argument. If array is empty, return the message "error - passed in array is empty". Otherwise, return a boolean value depending on the result of the check. Use the students array and the string "Jane" as arguments when testing.

let empty = []

function elementChecker(array, element){
    if(array.length == 0){
        console.log ("error - passed in array is empty")
    } else{
        console.log (array.includes(element))
    }
}

elementChecker(students,"Jane")
elementChecker(empty,"Jane")


/*
Create a function named checkAllStringsEnding that will accept a passed in array and a character. The function will do the ff:


if array is empty, return "error - array must NOT be empty"
if at least one array element is NOT a string, return "error - all array elements must be strings"
if 2nd argument is NOT of data type string, return "error - 2nd argument must be of data type string"
if 2nd argument is more than 1 character long, return "error - 2nd argument must be a single character"
if every element in the array ends in the passed in character, return true. Otherwise return false.

Use the students array and the character "e" as arguments when testing.
*/
const checkAllStringsEnding = (array, char) => {
	if(array.length === 0){
		console.log("error - array must NOT be empty")
	}

	if(array.some(element => typeof element !== "string")){
		console.log("error - all array elements must be strings")
	}

	if(typeof char !== "string"){
		console.log("error - 2nd argument must be of data type string")
	}

	if(char.length > 1){
		console.log("error - 2nd argument must be a single character")
	}

	console.log(array.every(element => element[element.length-1] === char))
	return array.every(element => element[element.length-1] === char)
}


checkAllStringsEnding(students, "e")


//Create a function named stringLengthSorter that will take in an array of strings as its argument and sort its elements in an ascending order based on their lengths. If at least one element is not a string, return "error - all array elements must be strings". Otherwise, return the sorted array. Use the students array to test.
function check(x) {
    return x.every(i => (typeof i === "string"));
}

function stringLengthSorter(array){
    if(check(array) == true){
        console.log(array.sort(function(a, b){return a.length - b.length}))
    } else{
        console.log("error - all array elements must be strings")
    }
}

let combo = ["string", 1, 7, "string 2"]
stringLengthSorter(students)
stringLengthSorter(combo)


/*



Create a function named startsWithCounter that will take in an array of strings and a single character. The function will do the ff:



if array is empty, return "error - array must NOT be empty"
if at least one array element is NOT a string, return "error - all array elements must be strings"
if 2nd argument is NOT of data type string, return "error - 2nd argument must be of data type string"
if 2nd argument is more than 1 character long, return "error - 2nd argument must be a single character"
return the number of elements in the array that start with the character argument, must be case-insensitive

Use the students array and the character "J" as arguments when testing.
*/
const startsWithCounter = (array, char) => {
	if(array.length === 0){
		console.log("error - array must NOT be empty")
	}

	if(array.some(element => typeof element !== "string")){
		console.log("error - all array elements must be strings")
	}

	if(typeof char !== "string"){
		console.log("error - 2nd argument must be of data type string")
	}

	if(char.length > 1){
		console.log("error - 2nd argument must be a single character")
	}

    console.log(array.every(element => element[0].toLowerCase() === char.toLowerCase()))
	return array.every(element => element[0].toLowerCase() === char.toLowerCase())
}

startsWithCounter(students, "J")


//Create a function named randomPicker that will take in an array and output any one of its elements at random when invoked. Pass in the students array as an argument when testing.

function randomPicker(array){
    r = Math.trunc(Math.random() * (array.length))
    if (r === array.length){
        randomPicker(array)
    }else {
        return array[r]
    }
}

console.log(randomPicker(students))